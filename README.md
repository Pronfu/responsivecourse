# Responsive Course Example #

If you want to create your own course and course dashboard you may find that the embed video doesn't look good on all screens. This repo fixes that. No matter what height or width the embed video can be seen well.

## FAQ ##

### How do I change the video that is on the page? ###
In each page is the line ` <iframe src='https://www.youtube.com/embed/VuN8qwZoego' frameborder='0' allowfullscreen></iframe>` and you replace that line is whatever code you want. If it isn't an iframe then you will have to change the `.embed-container iframe` in [style.css](https://bitbucket.org/Pronfu/responsivecourse/src/master/css/style.css).

### Do you own the embed video / is the embed video under Unlicense? ###
I do not own the videos they are copyright of [Wes Bos](https://wesbos.com/) and his [Javascript 30 course](https://javascript30.com/), he has posted them to YouTube so I'm using them as an example.

### Why are posting this under Unlicense, do you know Unlicense isn't...... ###
I post this and other things under [Unlicense](https://unlicense.org/) since the license is in plain English and I want to fully make this open source so you can use it in anyway you want without having to worry about reading over legal text or talking to a lawyer. 

### I'm a lawyer and I want to talk to you about using this with a client I'm representing. ###
Please email me (my email address if in this readme) and I will be happy to talk. I'm happy to sign this under another license exclusive to your client. Please keep it friendly and remember that there is another person (who isn't a lawyer) on the other end. If you want to talk over video conference, in-person, or by phone I'm happy to do so as well, please email me to arrange it.

## Installation ##

[Clone this repo](https://Pronfu@bitbucket.org/Pronfu/responsivecourse.git) then you can put it into any folder you want and customize it however you want.


## Special Thanks To  ##

[Chota framework](https://jenil.github.io/chota/) for the easy to use columns.
[HTML5 Boilerplate](https://html5boilerplate.com/) for the homepage template.

## Contributing ##

Pull requests and issues are welcome. If you don't have an account on Bitbucket you can email me to discuss what you think needs changing.

## Live Demo ##

[https://projects.gregoryhammond.ca/responsive-course/](https://projects.gregoryhammond.ca/responsive-course/)

## Contact Me? ##

If you need to contact me for some reason about this repo, just want to say hello, or want to hire me to do something similar for you, there are numerous ways you can contact me.

The most direct way is email, hello[at]gregoryhammond[dot]ca is my email.

## License ##

[Unlicense](https://unlicense.org/)